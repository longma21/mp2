import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './App.css';
import axios from 'axios';
import { BrowserRouter as Router, Route, Link, Routes, useParams, useNavigate } from 'react-router-dom';

const FilterBar = ({ onFilterChange }) => {
  const [isAdult, setIsAdult] = useState(false);
  const [minRating, setMinRating] = useState(0);
  const [minId, setMinId] = useState(0);

  useEffect(() => {
    onFilterChange(isAdult, minRating, minId);
  }, [isAdult, minRating, minId, onFilterChange]);

  return (
    <div className="filters">
      <label>
        <input 
          type="checkbox" 
          checked={isAdult}
          onChange={e => setIsAdult(e.target.checked)}
        />
        Adult Movies Only
      </label>
      
      <label>
        Minimum Rating:
        <input 
          type="number"
          value={minRating}
          onChange={e => setMinRating(Number(e.target.value))}
        />
      </label>

      <label>
      Minimum ID:
      <input 
        type="number"
        value={minId}
        onChange={e => setMinId(Number(e.target.value))}
      />
      </label>
    </div>
  );
};


const SearchBar = (props) => {
  const [searchTerm, setSearchTerm] = useState('');
  const [sortBy, setSortBy] = useState('name');
  const [order, setOrder] = useState('ascending');

  const handleInputChange = (event) => {
    setSearchTerm(event.target.value);
    if (props.onSearch) {
      props.onSearch(event.target.value, sortBy, order);
    }
  };

  const handleSortChange = (event) => {
    setSortBy(event.target.value);
    if (props.onSearch) {
      props.onSearch(searchTerm, event.target.value, order);
    }
  };

  const handleOrderChange = (newOrder) => {
    setOrder(newOrder);
    if (props.onSearch) {
      props.onSearch(searchTerm, sortBy, newOrder);
    }
  };

  return (
    <div>
      <div className="SearchInput">
        <input
          type="text"
          placeholder="Search Bar"
          value={searchTerm}
          onChange={handleInputChange}
          style={{ fontSize: '1.5em', padding: '10px' }}
        />
      </div>
      <div className="SortDropdown">
        <select value={sortBy} onChange={handleSortChange}>
          <option value="name">Name</option>
          <option value="id">ID</option>
          <option value="popularity">Popularity</option>
        </select>
      </div>
      <div className="OrderButtons">
        <button 
          onClick={() => handleOrderChange('ascending')}
          style={order === 'ascending' ? { background: 'blue', color: 'white' } : {}}
        >
          Ascending
        </button>
        <button 
          onClick={() => handleOrderChange('descending')}
          style={order === 'descending' ? { background: 'blue', color: 'white' } : {}}
        >
          Descending
        </button>
      </div>
    </div>
  );
};

const MovieDetail = () => {
  const { id } = useParams();  // 获取当前电影的ID
  const navigate = useNavigate();

  const [movie, setMovie] = useState({});

  useEffect(() => {
    const fetchMovieDetail = async () => {
      const apiKey = process.env.REACT_APP_API_KEY;
      const response = await axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=${apiKey}`);
      setMovie(response.data);
    };

    fetchMovieDetail();
  }, [id]);

  return (
    <div className="MovieDetail">
      <button onClick={() => navigate(`/movie/${Number(id) - 1}`)} className="prev">Previous Movie</button>
      <div className="content-container">
        <h2>{movie.original_title}</h2>
        <img src={`https://image.tmdb.org/t/p/w500${movie.backdrop_path}`} alt={movie.original_title} />
        <p>{movie.overview}</p>
      </div>
      <button onClick={() => navigate(`/movie/${Number(id) + 1}`)} className="next">Next Movie</button>
    </div>
  );
};

const Gallery = ({ movies: initialMovies }) => {
  const [movies, setMovies] = useState(initialMovies);

  const handleFilterChange = (isAdult, minRating, minId) => {

    if (initialMovies.length > 0) {
      console.log(`ID: ${initialMovies[0].id}, Title: ${initialMovies[0].original_title}, Runtime: ${initialMovies[0].runtime}`);
    }
  
  
    const filteredMovies = initialMovies.filter(movie => 
      (isAdult ? movie.adult : true) && movie.vote_average >= minRating && movie.id >= minId
    );
    setMovies(filteredMovies);
  };

  return (
    <div className="Gallery">
      <FilterBar 
        onFilterChange={handleFilterChange}
      />
      <div className="movie-gallery">
        {movies.map(movie => (
          <div key={movie.id} className="gallery-item">
            {movie.backdrop_path ? (
              <img src={`https://image.tmdb.org/t/p/w500${movie.backdrop_path}`} alt={movie.original_title} />
            ) : (
              <div className="PlaceholderImage">No Image Available</div>
            )}
            <div className="gallery-movie-title">{movie.original_title}</div>
          </div>
        ))}
      </div>
    </div>
  );
};


SearchBar.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

function App() {

  const [movies, setMovies] = useState([]);
  const [searchedMovies, setSearchedMovies] = useState([]);
  const [allMovies, setAllMovies] = useState([]);

  useEffect(() => {
    const fetchAllMovies = async () => {
      const baseURL = 'https://api.themoviedb.org/3/movie/popular';
      const apiKey = process.env.REACT_APP_API_KEY;
    
      let allMovies = [];
      let page = 1;
    
      while (true) {
        const url = `${baseURL}?api_key=${apiKey}&page=${page}`;
        try {
          const response = await axios.get(url);
          allMovies = [...allMovies, ...response.data.results];
    
          // Check if there are more pages to fetch
          if (page >= 50) {
            break;
          }
          page++;
        } catch (error) {
          console.error("Error fetching movies for page:", page, error);
          break;  // Break the loop if there's an error
        }
      }
    
      setAllMovies(allMovies);
    };

    fetchAllMovies();
  }, []);

  const handleSearch = async (term, sortBy, order) => {

    const baseURL = 'https://api.themoviedb.org/3/search/movie';
    const apiKey = process.env.REACT_APP_API_KEY;
    const url = `${baseURL}?api_key=${apiKey}&query=${term}`;

    try {
      const response = await axios.get(url);
      let fetchedMovies = response.data.results;

      fetchedMovies.sort((a, b) => {
        let compareValueA;
        let compareValueB;

        switch (sortBy) {
          case 'name':
            compareValueA = a.original_title;
            compareValueB = b.original_title;
            break;
          case 'id':
            compareValueA = a.id;
            compareValueB = b.id;
            break;
          case 'popularity':
            compareValueA = a.popularity;
            compareValueB = b.popularity;
            break;
          default:
            return 0;
        }

        if (order === 'ascending') {
          return compareValueA > compareValueB ? 1 : -1;
        } else {
          return compareValueA < compareValueB ? 1 : -1;
        }
      });

      setMovies(fetchedMovies);
      setSearchedMovies(fetchedMovies);
    } catch (error) {
      console.error("Error fetching movies:", error);
    }

    console.log('Movies:', movies);
  };

  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <h1>My App</h1>
          <nav>
            <Link to="/" className="nav-link">Search</Link>
            <Link to="/gallery" className="nav-link">Gallery</Link>
          </nav>
        </header>

        <Routes>
          <Route path="/" element={
            <div>
              <SearchBar onSearch={handleSearch} />
              <div className="MovieList">
                {searchedMovies.map(movie => (
                  <Link to={`/movie/${movie.id}`} key={movie.id} className="MovieItem">
                    {movie.backdrop_path ? (
                      <img src={`https://image.tmdb.org/t/p/w500${movie.backdrop_path}`} alt={movie.original_title} />
                    ) : (
                      <div className="PlaceholderImage">No Image Available</div>
                    )}
                    <div className="MovieDetails">
                      <div className="MovieTitle">{movie.original_title}</div>
                      <div className="MovieId">ID: {movie.id}</div>
                      <div className="MoviePopularity">Popularity: {movie.popularity.toFixed(2)}</div>
                    </div>
                  </Link>
                ))}
              </div>
            </div>
          }/>
          <Route path="/gallery" element={<Gallery movies={allMovies} />} />
          <Route path="/movie/:id" element={<MovieDetail />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
